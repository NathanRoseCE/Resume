# Resume

## Not Nathan

If you are not me and want to see a resume, look at: `Output/Digital.pdf`, if you're printing it grab the 
printable one. Digital just uses links, and the printable puts a more concise info


## Nathan

If you are me then you're probably wanting to generate a new resume, in which case you left yourself a nice `build.py` file.

ensure pdflatex and python3 are installed

To build:
2. `python3 -m venv venv`
3. `source venv/bin/activate`
4. `python3 -m pip install -r requirements.txt`
5. `python3 build.py`
